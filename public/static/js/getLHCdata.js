$(document).ready(function() {
    var MAX_RED = 0;
    var MIN_RED = 0;
    var red;
    var red1;
    var red2;
    var MAX_BLUE = 0;
    var MIN_BLUE = 0;
    var blue;
    var BLUE255;
    var MAX_GREEN = 0;
    var MIN_GREEN = 0;
    var green;
    var GREEN255;

    function get_smallest (num1, num2) {
        /*
         *Compares two numbers and returns the smallest one
         */
        if (typeof(num1) !== "number") return num2
        else if (typeof num2 !=="number") return num1
        else if (num1 < num2) return num1
        else return num2
    }

    function get_biggest (num1, num2) {
        /*
         *Compares two numbers and returns the biggest one
         */
        if (typeof(num1) !== "number") return num2
        else if (typeof num2 !== "number") return num1
        else if (num1 > num2) return num1
        else return num2
    }

    function rescale255(value, min_value, max_value){
        // Put the "variable" value in a scale between 0 and the "maximum" value.
        return Math.floor(Math.abs((value - min_value) * (255 / (max_value - min_value))));
    }

    function on_beam1_intensity (event, value) {
        red1 = parseFloat(value.value);
        if (isNaN(red1)) red1 = 0;
    }
    $(window).bind('beam1_intensity', on_beam1_intensity);

    function on_beam2_intensity (event, value) {
        red2 = parseFloat(value.value);
        if (isNaN(red2)) red2 = 0;
        red = Math.abs(parseFloat(red1) + parseFloat(red2));
        if (isNaN(red)) red = 0;
        MAX_RED = get_biggest(red, MAX_RED);
        MIN_RED = get_smallest(red, MIN_RED);
        RED255 = rescale255(red, MIN_RED, MAX_RED);
        changeColor($('.red'), RED255, 'red');
        changeBackgroundColor($('div#variables div.red-scale'), RED255, 'red');
        $("td.protons span").text(red.toPrecision(5));
    }
    $(window).bind('beam2_intensity', on_beam2_intensity);

    function on_acceleration_energy (event, value) {
        $(".lhc-acceleration-energy").html(parseFloat(value.valueInTeV).toPrecision(5) + " TeV &mdash;");
    }
    $(window).bind('acceleration_energy', on_acceleration_energy);

    function on_acceleration_mode (event, value) {
        $(".lhc-accelerator-mode").html(value.value + " &mdash;");
    }
    $(window).bind('acceleration_mode', on_acceleration_mode);

    function on_beam_mode (event, value) {
        since = new Date(parseInt(value.since));
        $(".lhc-beam-mode").html(value.value + " <span class='since'>" + since + "</span>");
        $(".since").easydate({locale: $.easydate.locales.frFR});
    }
    $(window).bind('beam_mode', on_beam_mode);


    function on_runconfig (event, value) {
        // Received information about the run configuration
        // console.log("run configuration: " + value);
        // FILL NUMBER
        //$("#fill-nb span").text(value.currentFill);
        //$(".lhc-beam-fill").text("Remplissage Nº : " + value.currentFill);
        
        // TOTAL ENERGY = BLUE
        if (value.currentBeamStoredEnergyKwh) {
            blue = parseFloat(value.currentBeamStoredEnergyKwh);
            if (isNaN(blue)) blue = 0;
            MAX_BLUE = get_biggest(blue, MAX_BLUE);
            MIN_BLUE = get_smallest(blue, MIN_BLUE);
            BLUE255 = rescale255(blue, MIN_BLUE, MAX_BLUE);
            changeColor($('.blue'), BLUE255, 'blue');
            changeBackgroundColor($('div#variables div.blue-scale'), BLUE255, 'blue');
            $("td.energy span").text(blue.toExponential(4) + " kW/h");
        } else {
            $("td.energy span").text("0");
            BLUE255 = 0;
            changeColor($('.blue'), BLUE255, 'blue');
            changeBackgroundColor($('div#variables div.blue-scale'), BLUE255, 'blue');
        }
        
        // COLLISIONS
        if (value.instantNumberOfCollisions) {
            green = parseFloat(value.instantNumberOfCollisions);
            if (isNaN(green)) green = 0;
            MAX_GREEN = get_biggest(green, MAX_GREEN);
            MIN_GREEN = get_smallest(green, MIN_GREEN);
            GREEN255 = rescale255(green, MIN_GREEN, MAX_GREEN);
            changeColor($('.green'), GREEN255, 'green');
            changeBackgroundColor($('div#variables div.green-scale'), GREEN255, 'green');
            $("td.collisions span").text(green.toPrecision(5));
        } else {
            GREEN255 = 0;
            changeColor($('.green'), GREEN255, 'green');
            changeBackgroundColor($('div#variables div.green-scale'), GREEN255, 'green');
            $("td.collisions span").text("0");
        }
    }
    $(window).bind('runconfig', on_runconfig);

    function on_page1 (event, value) {
        $("#page1 span").text(value.value);
        $(".lhc-page-1-comments").text(value.value);
    }
    $(window).bind('page1', on_page1);

    var detectedTransport = null;
    var socket = $.atmosphere;
    var subSocket;

  //  var broadcastServiceUrl = '//dashboard.web.cern.ch/broadcast';
    var topicName = 'LDCP-PRODUCTION';

    var subscriptionsList = [
             "bcast://lcdp/dip/acc/LHC/RunControl/BeamMode"
            ,"bcast://lcdp/dip/acc/LHC/RunControl/MachineMode"
            ,"bcast://lcdp/dip/acc/LHC/RunControl/Page1"
            ,"bcast://lcdp/dip/acc/LHC/Beam/Intensity/Beam1"
            ,"bcast://lcdp/dip/acc/LHC/Beam/Intensity/Beam2"
            ,"bcast://lcdp/dip/acc/LHC/Beam/Energy"
            ,"bcast://lcdp/dip/acc/LHC/RunControl/RunConfiguration"
            ];
    
    function openConnection() {
                        console.log("Sending subscribe");
                        $("#CERNLDCPproxy")[0].contentWindow.postMessage({
                            type : "request",
                            request : "addsubscriptions",
                            content : {
                                topic : topicName ,
                                subscriptions : subscriptionsList 
                            }
                        }, "http://dashboard.web.cern.ch");

                        console.log("Sending open connection");
                        $("#CERNLDCPproxy")[0].contentWindow.postMessage({
                            type : "request",
                            request : "openconnection",
                            content : {
                                topic : topicName ,
                                maxReconnectOnClose : 10
                            }
                        }, "http://dashboard.web.cern.ch");
                        

            }

    function closeConnection(topicToClose) {
                $("#proxy")[0].postMessage({
                    type : "request",
                    request : "closeconnection",
                    content : {
                        topic : topicToClose
                    }
                }, proxyDomain);
            }


    window.addEventListener('message', function(message) {
                    console.log("container received message:");
                    console.log(message.data);
                    

                    if (message.data.type === "request") {
                        proxyFrame.postMessage(message.data, proxyDomain);
                    } else if (message.data.type === "init") {
                        openConnection();
                    } else if (message.data.type === "data") {
                        var topicReceived = message.data.content.topicName;
                        var output = "<p>Received updates for topic '" + topicReceived
                            + "' : <br/>";
                        for (var i = 0; i < message.data.content.updates.length; i++) {
                            var update = message.data.content.updates[i];
                            output = output + " - " + update.publicationName + " => "
                                + JSON.stringify(update.newValue) + "<br/>";
                        }
                    

                       if (live == true && message.data.content && message.data.content.updates && message.data.content.updates.length > 0) {
                               $.each(message.data.content.updates, function(index, updateData) {
                                // Strip the initial 'bcast://lcdp/' prefix
                                switch((updateData.publicationName).substring(13)) {
                                case 'dip/acc/LHC/Beam/Intensity/Beam1':
                                  $(window).trigger('beam1_intensity', updateData.newValue);
                                  break;
                                case 'dip/acc/LHC/Beam/Intensity/Beam2':
                                  $(window).trigger('beam2_intensity', updateData.newValue);
                                   break;
                                case 'dip/acc/LHC/Beam/Energy':
                                    $(window).trigger('acceleration_energy', updateData.newValue);
                                    break;
                                case 'dip/acc/LHC/RunControl/BeamMode':
                                    $(window).trigger('beam_mode', updateData.newValue);
                                    break;
                                case 'dip/acc/LHC/RunControl/MachineMode':
                                    $(window).trigger('acceleration_mode', updateData.newValue);
                                    break;
                                case 'dip/acc/LHC/RunControl/Page1':
                                    $(window).trigger('page1', updateData.newValue);
                                    break;
                                case 'dip/acc/LHC/RunControl/RunConfiguration': 
                                    // lastFillMaxIntensityBeam2, currentFill, 
                                    // lastFillMaxIntensityBeam1, lastFill, 
                                    // lastFillNbCollisions, lastFillMaxEnergy
                                    // instantNumberOfCollisions
                                    $(window).trigger('runconfig', updateData.newValue);
                                    break;
                               }
                               } );
                        }
                      } else if (message.data.type === "info") {
                        if(window.console){
                            console.log("Subscription info message : " + message.data.content );
                        }
                      }
                
               }, false );

});

