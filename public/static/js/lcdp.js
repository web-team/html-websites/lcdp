// TIME FUNCTIONS {{{
var live = true;

function pad2(number) {
     return (number < 10 ? '0' : '') + number;
}

$.easydate.locales.frFR = { 
    "future_format": "%s %t", 
    "past_format": "%s %t", 
    "second": "seconde", 
    "seconds": "secondes", 
    "minute": "minute", 
    "minutes": "minutes", 
    "hour": "heure", 
    "hours": "heures", 
    "day": "jour", 
    "days": "jours", 
    "week": "semaine", 
    "weeks": "semaines", 
    "month": "mois", 
    "months": "mois", 
    "year": "an", 
    "years": "ans", 
    "yesterday": "depuis hier", 
    "tomorrow": "demain", 
    "just now": "depuis peu", 
    "ago": "depuis", 
    "in": "dans" 
};


// DATE {{{
    var month_names = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
    var day_names = ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"];
    var seconds = 0;
    var mydate = new Date();
    mydate.setHours(mydate.getUTCHours() + 2); // moves the timezone offset to get CERN time
// }}}

function getTime () {
    /* Updates the current time, in UTC format */
    mydate = new Date();

    if((mydate - start) > 3600000){
       var iframesrc = $("iframe#flux").attr("src"); 
       $("iframe#flux").attr("src", iframesrc); 
       start = new Date();
    }

    if(mydate.getTimezoneOffset() == -60) {
        mydate.setHours(mydate.getUTCHours()+1);
    } else if(mydate.getTimezoneOffset() == -120) {
        mydate.setHours(mydate.getUTCHours()+2);
    }
}
function displayTime() {
    /* Displays current time in the timestamp div */
    getTime();
    $(".datestamp").html(day_names[mydate.getDay()] + " " + mydate.getDate() + " " + month_names[mydate.getMonth()] + " " + mydate.getFullYear());
    $(".timestamp").html(pad2((mydate.getHours())) + ":" + pad2(mydate.getMinutes()) + ":" + pad2(mydate.getSeconds()) + "&trade;");
}

function backToThePast(gotodate, time){
    live = false;
    now = new Date();
    // CHOSEN DATE
    date = gotodate.split("-");
    mydate.setYear(date[0]);
    mydate.setMonth(date[1] -1);
    mydate.setDate(date[2]);
    // stops live date
    clearInterval(updateTime);

    // COMPARES CHOSEN DATE WITH PRESENT
    if (mydate > now){ ////// IF FUTURE

        // FONDU AU NOIR
        $(".red, .green, .blue").animate({
            'color': 'rgb(0, 0, 0)', 
        }, "slow");

        // whitens background font and elements in DATA window
        setTimeout(function(){
            $(".green, div#variables th").animate({
                'color': 'rgb(255, 255, 255)', 
            }, "slow");
            $("div#variables div#equalizers div").animate({
                'background-color': 'rgb(255, 255, 255)', 
            }, "slow");
        });

        // removes data from DATA window
        $("div#variables td span").text("");

    } else{ ////// IF PAST, BACKGROUND TAKES COMPONENT COLORS
        // FONDU AU NOIR
        $(".red, .green, .blue").css("color", "#000000")
        //$(".red, .green, .blue").animate({
            //'color': 'rgb(0, 0, 0)', 
        //}, "slow");
        // RÉCUPÉRATION DES COULEURS DU JOUR CONCERNÉ
        red   = $("div#history tr#history_" +gotodate+ " div.red-scale").attr("data-tint");
        green = $("div#history tr#history_" +gotodate+ " div.green-scale").attr("data-tint");
        blue  = $("div#history tr#history_" +gotodate+ " div.blue-scale").attr("data-tint");

        // CHANGE LES COULEURS EN FONCTION DES DONNÉES DE L'HISTORIQUE
        setTimeout(function(){
            changeColor($('.red'), red, 'red');
            changeColor($('div#variables #th-red'), 255, 'red');
            changeBackgroundColor($('div#variables div.red-scale'), red, 'red');
            changeColor($('.green'), green, 'green');
            changeColor($('div#variables #th-green'), 255, 'green');
            changeBackgroundColor($('div#variables div.green-scale'), green, 'green');
            changeColor($('.blue'), blue, 'blue');
            changeColor($('div#variables #th-blue'), 255, 'blue');
            changeBackgroundColor($('div#variables div.blue-scale'), blue, 'blue');
        }, 300);
        $("div#variables div#equalizers div").animate({
            'background-color': 'rgb(0, 0, 0)', 
        }, "slow");

        // RÉCUPÉRATION DES DONNÉES DU JOUR CONCERNÉ
        past_protons = parseFloat($("div#history tr#history_" +gotodate+ " div.red-scale").attr("data-value")).toPrecision(5);
        past_collisions = parseFloat($("div#history tr#history_"+gotodate+" div.green-scale").attr("data-value")).toPrecision(5);
        past_energy = parseFloat($("div#history tr#history_" +gotodate+ " div.blue-scale").attr("data-value")).toExponential(4);
        if (isNaN(past_protons)) past_protons = "0";
        if (isNaN(past_collisions)) past_collisions = "0";
        if (isNaN(past_energy)) past_energy = "0";
        // marque les données chiffrées dans fenêtre DONNÉES
        $("td.protons span").text(past_protons);
        $("td.collisions span").text(past_collisions);
        $("td.energy span").text(past_energy + " kW/h");
    }
    
    // Displays past date in background
    $(".timestamp").hide();
    $(".datestamp").html(day_names[mydate.getDay()] + " " + mydate.getDate() + " " + month_names[mydate.getMonth()] + " " + mydate.getFullYear());
    //$(".timestamp").html(day_names[mydate.getDay()] + " " + mydate.getDate() + " " + month_names[mydate.getMonth()] + " " + mydate.getFullYear());

    // Hides all news and live info
    $("div#news ul").hide();
    $("div#background .lhc-beam-mode").hide();
    $("div#background .lhc-accelerator-mode").hide();
    $("div#background .lhc-page-1-comments").hide();
    $("div#background .lhc-acceleration-energy").hide();
    // Displays the corresponding news
    $("div#news ul#news_" + gotodate).show();

    // Deactivate dates in history
    $('div#history tr').removeClass('active');
    // Activates chosen date in history
    $("div#history tr#history_" + gotodate).addClass('active');
    // Turn off the LIVE button
    $("div#clock a.clock-live").removeClass("active").addClass("inactive");
}
// }}}

// COLOR FUNCTIONS {{{ 
function changeColor(target, value, colour){
    // Apply a variable "value" to a color.
    var v = parseInt(value);
    if (isNaN(v)) v = 0;
    if (v < 100) v = 100;

    if (colour == 'red') {
        $(target).animate({
            'color': 'rgb('+ v +',0, 0)', 
        });
    } else if (colour == 'green') {
        $(target).animate({
            'color': 'rgb(0,'+ v +', 0)', 
        });
    } else if (colour == 'blue') {
        $(target).animate({
            'color': 'rgb(0,0,'+ v +')', 
        });
    }
}

function changeBackgroundColor(target, value, colour){
    // Apply a variable "value" to a color.
    var v = parseInt(value);
    if (isNaN(v)) v = 0;
    if (v < 100) v = 100;

    if (colour == 'red') {
        $(target).animate({
            'background-color': 'rgb('+ v +',0, 0)', 
        });
    } else if (colour == 'green') {
        $(target).animate({
            'background-color': 'rgb(0,'+ v +', 0)', 
        });
    } else if (colour == 'blue') {
        $(target).animate({
            'background-color': 'rgb(0,0,'+ v +')', 
        });
    }
}


function getPastRGB (target, color, value, min, max){
    tint = Math.round(Math.abs(value - min) * (255/(max - min)));
    if (isNaN(tint)) tint = 0;
    if (tint > 255) tint = 255;
    if (tint < 100) tint = 100;
    $(target).attr("data-tint", tint);

    if (color == "red") {
        $(target).css("backgroundColor", "rgb(" + tint  + ", 0, 0)");

    } else if (color == "green") {
        $(target).css("backgroundColor", "rgb(0, "+ tint +", 0)");

    } else if (color == "blue") {
        $(target).css("backgroundColor", "rgb(0, 0," + tint + ")");
    }
}

// }}}

/* Init {{{ */

function init () {
    getTime(); // Get current UTC time
    displayTime(); // Displays time in the timestamp div
    updateTime = setInterval(displayTime, 1000) 

    $("div.window").animate({height: "20px"}, 500, "easeOutCubic");
    $("div.window").addClass("collapse");
    
    // CHECKS IF THERE IS A TWEET TODAY
    today_date = mydate.getFullYear() + "-" + pad2(mydate.getMonth() + 1) + "-" +  pad2(mydate.getDate());
    today_tweet = $("ul#news_" + today_date) 
    if (today_tweet) {
        today_tweet.show();
        $('div#history tr#history_' + today_date).addClass('active');
    }
}

$(window).load(function () { init(); });

var start;
$(document).ready(function(){
    start = new Date;

    // WINDOW MANIPULATION {{{
    $("div.window")
        .draggable({
            start: function(event, ui){
                $('div.active').removeClass('active').addClass('inactive');
                $(this).addClass('active').removeClass("inactive");
            }, handle: "h2",
        })
        .click(function(e){
          $('div.active').removeClass('active').addClass('inactive');
          $(this).addClass('active').removeClass("inactive");

          var $target = $(e.target)
          if($target.is("h2")) {
              if ( $(this).is('.ui-draggable-dragging') ) {
                    return;
              }
              if ($(this).hasClass("collapse")){
                  $(this).animate({height: $(this).attr("data-height")}, 500, "easeOutCubic");
              } else {
                  $(this).animate({height: "20px"}, 500, "easeOutCubic");
              }
              $(this).toggleClass("collapse");
          }
        });


    // HISTORY
    $('div#history table.text a').click(function(e){
        e.preventDefault();
        // Reverberates the date
        gotodate = $(this).parent().parent().attr("id").split("_")[1];
        gototime = $(this).parent().parent().attr("data-time");
        backToThePast(gotodate, gototime);
    });
    $('div#history table.text a').hover(
        function(){
            $("tr.hover").removeClass("hover");
            $(this).parent().parent().addClass("hover");
        }, function(){
            $("tr.hover").removeClass("hover");
    });

    // CLOCK
    // MAKE LIVE
    $("div#clock a.clock-live").click(function(e){
        e.preventDefault();
        $(".timestamp").show();
        updateTime = setInterval(displayTime, 1000) 
        $("div#background .lhc-beam-mode").show();
        $("div#background .lhc-accelerator-mode").show();
        $("div#background .lhc-page-1-comments").show();
        //$("div#background .lhc-beam-fill").show();
        $("div#background .lhc-acceleration-energy").show();
        $("div#news ul").hide();
        $('div#variables th').removeClass("white");
        $("div#clock .clock-live").removeClass("inactive").addClass("active");
        $('div#history tr').removeClass('active');
        live = true;
    });
    


});

/* } */

/* vim: set foldmethod=marker foldmarker={,}: */
