/*
 * Utilities {
 */
Number.prototype.pad = function( width, fillChar, radix ){ 
    /*
     *Formats numbers to be of a given length
     *eg. 2 -> 02
     */
    var s = this.toString(radix || 10); 
    var f = (new Array( s.length < Math.abs(width) ? Math.abs(width) - s.length + 1 : 0)).join(fillChar || '0');  
    return width > 0 ? f + s : s + f 
};

function getBiggest (num1, num2) {
    /*
     *Compares two numbers and returns the biggest one
     */
    if (typeof(num1) !== "number") {
        return num2
    } else if (typeof num2 !=="number") {
        return num1
    } else if (num1 > num2) {
        return num1
    } else {
        return num2
    };
}

function getSmallest (num1, num2) {
    /*
     *Compares two numbers and returns the smallest one
     */
    if (typeof(num1) !== "number") {
        return num2
    } else if (typeof num2 !=="number") {
        return num1
    } else if (num1 < num2) {
        return num1
    } else {
        return num2
    };
}
/* } */

/*
 * Global variables {
 */

// Names of the XML variables
var Variables = [
    "alice-lhc-luminosity",
    "lhc-beam-energy",
    "lhc-filling-scheme",
    "lhc-hypercycle",
    "lhc-beam-mode",
    "lhc-beam-1-intensity",
    "lhcb-lhc-luminosity",
    "lhc-accelerator-mode",
    "atlas-lhc-luminosity",
    "lhc-beam-2-intensity",
    "cms-lhc-luminosity",
];

var variable1 = "lhc-beam-1-intensity";
var variable2 = "atlas-lhc-luminosity";
var variable3 = "lhc-beam-2-intensity";

var json = {}; // Holds the current values;
var source = "lhc"; // source for the data

// Computes time for then retrieving the XML  
var mydate = new Date();
mydate.setHours(mydate.getHours() + (mydate.getTimezoneOffset()/60)); // removes the timezone offset to get CERN time
mydate.setSeconds(0); // fixe le bug

var index = 0; // Holds the position in the XML

var minValues = {
    "alice-lhc-luminosity" : [],
    "lhc-beam-energy" : [],
    "lhc-filling-scheme" : [],
    "lhc-hypercycle" : [],
    "lhc-beam-mode" : [],
    "lhc-beam-1-intensity" : [],
    "lhcb-lhc-luminosity" : [],
    "lhc-accelerator-mode" : [],
    "atlas-lhc-luminosity" : [],
    "lhc-beam-2-intensity" : [],
    "cms-lhc-luminosity" : [],
};
var maxValues = {
    "alice-lhc-luminosity" : [],
    "lhc-beam-energy" : [],
    "lhc-filling-scheme" : [],
    "lhc-hypercycle" : [],
    "lhc-beam-mode" : [],
    "lhc-beam-1-intensity" : [],
    "lhcb-lhc-luminosity" : [],
    "lhc-accelerator-mode" : [],
    "atlas-lhc-luminosity" : [],
    "lhc-beam-2-intensity" : [],
    "cms-lhc-luminosity" : [],
};

/* } */

/*
 * Functions {
 */
function getTime () {
    /*
     *Displays current time in the timestamp div
     */
    var myDate = new Date();
    $(".timestamp").text(myDate.toDateString() + " " + myDate.toTimeString());
    window.setTimeout("getTime()", 1000);
}

function makeUrl () {
    /*
     *Returns the XML URL for the given date object
     */
    var newUrl = "lhc-webcast/";
    newUrl += mydate.getFullYear().pad(4) + "/";
    newUrl += (mydate.getMonth() + 1).pad(2) + "/";
    newUrl += mydate.getDate().pad(2) + "/";
    newUrl += mydate.getHours().pad(2) + "/";
    newUrl += source + "-";
    newUrl += mydate.getFullYear().pad(4);
    newUrl += (mydate.getMonth() + 1).pad(2);
    newUrl += mydate.getDate().pad(2);
    newUrl += mydate.getHours().pad(2);
    newUrl += mydate.getMinutes().pad(2);
    newUrl += mydate.getSeconds().pad(2);
    newUrl += ".xml";
    return newUrl;
}

function loadXML () {
    /*
     *Loads the XML file and update the global values.
     */
    $.ajax({
        url: "/proxy/" + makeUrl(),
        dataType: "xml",
        cache: false,
        complete : function(data, status) {
            json = xml2json(data.responseXML);
            updateData();
        }
    });
}

function xml2json (xml) {
    /*
     *Parses the XML and returns an object
     */
    var variables = {};
    
    $(xml).find('variable').each(function(){
        var name = $(this).attr('name');
        variables[name] = {
            'dippublication': $(this).attr('dippublication'),
            'dipfield': $(this).attr('dipfield'),
            'scale': $(this).attr('scale'),
            'unit': $(this).attr('unit'),
            'values': $(this).text().split(","),
        };
    });
    
    return(variables);
}

function changeColor(target, ble, factor){
    //target.css('opacity', ble.values[index]/factor);
    $.each(target, function(){
        $(this).animate({
            opacity: ble.values[index]/factor,
        });
    });
}

function changeColor2(target, value, colour){
    // Apply a variable "value" to a color.
    var v = parseInt(value);
    if (isNaN(v)) v = 255;

    target.each(function(){
        if (colour === 'R') {
            $(this).animate({
                'color': 'rgb('+ v +',0, 0)', 
            });
        } else if (colour === 'G') {
            $(this).animate({
                'color': 'rgb(0,'+ v +', 0)', 
            });
        } else if (colour === 'B') {
            $(this).animate({
                'color': 'rgb(0,0,'+ v +')', 
            });
        }
    });
}

function equalizer(target, value){
    // Visualize color variations with bars.
    $(target).animate({
        width: value,
    })
}

function rescale(variable, maximum){
    // Put the "variable" value in a scale between 0 and the "maximum" value.
    return Math.abs((json[variable]["values"][index] - minValues[variable]) 
           * (maximum/(maxValues[variable] - minValues[variable])));
}


function updateData () {

    // Sets threshold values
    for (var i in json) {
        var value = parseFloat(json[i]['values'][index]);
        minValues[i] = getSmallest(minValues[i], value);
        maxValues[i] = getBiggest(maxValues[i], value);
    };

    // Updates the values in the table on the page
    $("tr.alice-lhc-luminosity td.var-value").text(parseFloat(json["alice-lhc-luminosity"]['values'][index]).toPrecision(5));
    $("tr.lhc-beam-energy td.var-value").text(parseFloat(json["lhc-beam-energy"]['values'][index]).toPrecision(5));
    $("tr.lhc-filling-scheme td.var-value").text(json["lhc-filling-scheme"]['values'][index]);
    $("tr.lhc-hypercycle td.var-value").text(json["lhc-hypercycle"]['values'][index]);
    $("tr.lhc-beam-mode td.var-value").text(json["lhc-beam-mode"]['values'][index]);
    $("tr.lhc-beam-1-intensity td.var-value").text(parseFloat(json["lhc-beam-1-intensity"]['values'][index]).toPrecision(5));
    $("tr.lhcb-lhc-luminosity td.var-value").text(parseFloat(json["lhcb-lhc-luminosity"]['values'][index]).toPrecision(5));
    $("tr.lhc-accelerator-mode td.var-value").text(json["lhc-accelerator-mode"]['values'][index]);
    $("tr.atlas-lhc-luminosity td.var-value").text(parseFloat(json["atlas-lhc-luminosity"]['values'][index]).toPrecision(5));
    $("tr.lhc-beam-2-intensity td.var-value").text(parseFloat(json["lhc-beam-2-intensity"]['values'][index]).toPrecision(5));
    $("tr.cms-lhc-luminosity td.var-value").text(parseFloat(json["cms-lhc-luminosity"]['values'][index]).toPrecision(5));

    // Change the opacity of R, V and B elements
    var R = rescale(variable1, 255);
    var G = rescale(variable2, 255);
    var B = rescale(variable3, 255);
    
    // Shift the timing of the 3 variable changes
    changeColor2($('div.active div.red, div#background div.red, td.red'), R, 'R');
    window.setTimeout(function(){
       changeColor2($('div.active div.green, div#background div.green, td.green'), G, 'G');
    }, 333);
    window.setTimeout(function(){
       changeColor2($('div.active div.blue, div#background div.blue, td.blue'), B, 'B');
    }, 666);
    
    // Visualizes RGB values with colored bars
    equalizer($('div#red-scale'), R);
    equalizer($('div#green-scale'), G);
    equalizer($('div#blue-scale'), B);

    // Goes to the next value in the current XML or reload a new one.
    index += 1;
    if (index < 20) {
       setTimeout(updateData, 1000);
    } else {
       mydate.setTime(mydate.getTime() + 20000)
       loadXML();    
       index = 0;
    };
}
/* } */

/*
 * Init {
 */

function init () {
    
    // Assigns R, V and B to the three following variables
    $("tr." + variable1 + " td").addClass('red').css('font-family', 'chicagoFull');
    $("tr." + variable2 + " td").addClass('green').css('font-family', 'chicagoFull');
    $("tr." + variable3 + " td").addClass('blue').css('font-family', 'chicagoFull');

    getTime(); // Displays time in the timestamp div
    loadXML(); // Loads XML from LHC webcast
    
    var bt = '#bt-' + $('body').attr('id') + ':last-child';
    $(bt).addClass('current-page green');
    $(bt).before($(bt).clone().attr('class', 'current-page red').css({position: 'absolute', 'left': '1em', 'color': 'red'}));
    $(bt).after($(bt).clone().attr('class', 'current-page blue').css({position: 'absolute', 'left': '1em', 'color': 'blue'}));

    // fixMozillaZIndex=true;

}

$(window).load(function () { init(); });

$(document).ready(function(){
    // Windows manipulation
    $('div.window').resizable().draggable({
        start: function(event, ui) {
            $('div.active div.red').css('color', 'rgb(255, 0, 0)');
            $('div.active div.green').css('color', 'rgb(0, 255, 0)');
            $('div.active div.blue').css('color', 'rgb(0, 0, 255)');
            $('div.active').removeClass('active');
            $(this).addClass('active');
        },
        handle: 'h2', 
    });

    $('div.window').click(function(){
        $('div.active div.red').css('color', 'rgb(255, 0, 0)');
        $('div.active div.green').css('color', 'rgb(0, 255, 0)');
        $('div.active div.blue').css('color', 'rgb(0, 0, 255)');
        $('div.active').removeClass('active');
        $(this).addClass('active');
    });
 
    $('div.window h2').dblclick(function(){
        $(this).parent().toggleClass('collapse');
        //$($('div.text'), $(this)).toggleClass('hidden');
    });

    // Assign variables to colors using radio buttons
    var radio = $('table#var-table').find("input[type=radio]");
    radio.each(function(){
        $(this).click(function(){
            $('table#var-table td').css({'color': 'white', 'font-family': 'chicagoGreen' }).attr('class', '');
            
            variable1 = $('table#var-table input[name=variable1]:checked').val();
            variable2 = $('table#var-table input[name=variable2]:checked').val();
            variable3 = $('table#var-table input[name=variable3]:checked').val();
            $('tr.' + variable1 + ' td').addClass('red').css('font-family', 'chicagoFull');
            $('tr.' + variable2 + ' td').addClass('green').css('font-family', 'chicagoFull');
            $('tr.' + variable3 + ' td').addClass('blue').css('font-family', 'chicagoFull');
        });
    });
    
    // Apply variable colors on hover menu
    // $('div#menu a').each(function(){
    //     $('div#menu a').hover(function(){
    //         hover_menu = $(this).attr('id');
    //         $(this).addClass('current-page green');
    //         $(this).before($(this).clone().attr('class', 'current-page red').css({position: 'absolute', 'left': '1em', 'color': 'red'}));
    //         $(this).after($(this).clone().attr('class', 'current-page blue').css({position: 'absolute', 'left': '1em', 'color': 'blue'}));
    //     }, function(){
    //         $('#' + hover_menu + '.red').remove();
    //         $('#' + hover_menu + '.blue').remove();
    //         $('#' + hover_menu + '.green').attr('class', '').css('color', 'white');
    //     });
    // });


    // Pause/Resume sound
    $('div#tools a#mute').toggle(
        function(e){
            e.preventDefault();
            if ($f(0).id() == 'audio'){
                $f(0).pause();                
            } else {
                $f(3).pause();
            }
            $(this).text('[ ]');
        },
        function(e){
            e.preventDefault();
            if ($f(0).id() == 'audio'){
                $f(0).resume();
            } else {
                $f(3).resume();
            }
            $(this).text('[×]');

        }
    );
});

/* } */

/* vim: set foldmethod=marker foldmarker={,}: */
